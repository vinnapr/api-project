# API Project

This is API project created with Node Js, Express, Sequelize & Postgresql

#### Configuration
Base configuration file is located inside `/config` directory.

#### Migrations & Seed
- To create new migration script, use `yarn sequelize-cli model:generate --name {NAME} --attributes columnName:dataType`
- To run the migration script, `yarn sequelize-cli db:migrate`
  * If you wish to undo most recent migration: `yarn sequelize-cli db:migrate:undo`

- To create new seed, use `yarn sequelize-cli seed:generate --name {NAME}`
- To runing the seeds, use `yarn sequelize-cli db:seed:all`
  * If you wish to undo most recent seed: `yarn sequelize-cli db:seed:undo`
  * If you wish to undo all seeds: `yarn sequelize-cli db:seed:undo:all`

#### CLI Commands
Here's list of commands you can use:
- `yarn start` run app server