const express = require('express');
const userModule = require('./users/routes');
const companyModule = require('./companies/routes');
const jsend = require('jsend');

const app = express();

app.use(userModule);
app.use(companyModule);
app.use(jsend());

module.exports = app;