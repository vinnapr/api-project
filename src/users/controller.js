const Users = require('../../db/models').users;
const Companies = require('../../db/models').companies;
const config = require('../../config/configJWT');

const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

const UserController = {}

// User Registration
UserController.signup = async (req, res) => {
    const data = req.body;
    data.password = await bcrypt.hash(data.password, saltRounds);
    const insert = await Users.create(data);
    return res.json({ status: "success", data: insert });
};

// User Login
UserController.signin = async (req, res, next) => {
    const {username, password} = req.body;
    const user = await Users.findOne({ where: {username}});
    if (!user) {
        const err = 'Incorrect Username';
        return next(err);
    }
    const checkPassword = await bcrypt.compare(password, user.password);
    if (!checkPassword) {
        const err = 'Incorrect Password';
        return next(err);
    }
    const expiresIn = 24 * 60 * 60;
    const accessToken = jwt.sign({ username }, config.secret, {
        expiresIn: expiresIn
    });
    const insert = await user.update({ refreshToken: accessToken });
    return res.json({ status: "success", data: user});
};

UserController.list = async (req, res) => {
    const listUsers = await Users.findAndCountAll({
        include: [{
            model: Companies,
            as: 'companies',
        }],
    });
    return res.json({ data: listUsers });
};

UserController.get = async (req, res, next) => {
    const { id } = req.params;
    const user = await Users.findOne({ 
        where: { id }, 
        include: [{
            model: Companies,
            as: 'companies',
        }],
    });
    if (!user) {
        const err = "Data not found";
        return next(err);
    }
    return res.json({ status: "success", data: user });
};

UserController.edit = async (req, res, next) => {
    const { id } = req.params;
    const data = req.body;
    const user = await Users.findOne({ where: {id} });
    if (!user) {
        const err = "Data not found";
        return next(err);
    }
    data.updateAt = new Date();
    user.changed('updatedAt', true);
    const update = await oneUser.update(data);
    return res.json({ status: "success", data: update });
};

module.exports = UserController;