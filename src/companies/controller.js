const Users = require('../../db/models').users;
const Companies = require('../../db/models').companies;

const CompanyController = {};

CompanyController.list = async (req, res) => {
    const listCompanies = await Companies.findAndCountAll({
        include: [{
            model: Users,
            as: 'users',
        }],
    });
    return res.json({ status: "success", data: listCompanies });
};

CompanyController.get = async (req, res, next) => {
    const { id } = req.params;
    const company = await Companies.findOne({
        where: { id },
        include: [{
            model: Users,
            as: 'users',
        }],
    });
    if (!company) {
        const err = 'Data not found';
        return next(err);
    }
    return res.json({ status: "success", data: company });
};

CompanyController.edit = async (req, res, next) => {
    const { id } = req.params;
    const data = req.body;
    const company = await Companies.findOne({ where: { id } });
    if (!company) {
        const err = 'Data not found';
        return next(err);
    }
    data.updateAt = new Date();
    company.changed('updatedAt', true);
    const update = await company.update(data);
    return res.json({ status: "success", data: update });
};

CompanyController.create = async (req, res, next) => {
    const data = req.body;
    const insert = await Companies.create(data);
    return res.json({ status: "success", data: insert });
};

CompanyController.delete = async (req, res, next) => {
    const { id } = req.params;
    const data = req.body;
    const company = await Companies.findOne({ where: { id } });
    if (!company) {
        const err = 'Data not found';
        return next(err);
    }
    const deleteCompany = await company.destroy(data);
    return res.json({ status: "succes", data: deleteCompany });
};

module.exports = CompanyController;