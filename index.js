const userModule = require('./src/users/routes');
const companyModule = require('./src/companies/routes');

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(userModule);
app.use(companyModule);

const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(`App running on port ${port}`)
});