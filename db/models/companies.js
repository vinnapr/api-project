'use strict';
module.exports = (sequelize, DataTypes) => {
  const companies = sequelize.define('companies', {
    userId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});
  companies.associate = function(models) {
    // associations can be defined here
    companies.belongsTo(models.users, {
      onDelete: 'CASCADE',
      foreignKey: 'userId',
      as: 'users',
    });
  };
  return companies;
};