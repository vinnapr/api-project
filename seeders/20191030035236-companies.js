module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('companies', [{
      userId: 1,
      name: 'Qatrosku',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      userId: 1,
      name: 'Qatros Teknologi Nusantara',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      userId: 1,
      name: 'Binar Academy',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('companies', null, {});
  },
};